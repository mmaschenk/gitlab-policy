---
title: Why is there no gitlab-pages service?
parent: FAQ
nav_exclude: true
---

##### Gitlab pages is really useful. Why is it not available?

One of the things where our local server is not as attractive as the public commercial gitlab service is that we at TU Delft unfortunately cannot afford to blindly add all functions to our setup: the amount of maintenance personnel required to keep this running is simply not available!

For that reason we are currently not offering gitlab-pages on our setup.

See also [this page]({% link faq/why.md %}#reasons-not-to-use-gitlabtudelft) and specifically [this entry]({% link faq/why.md %}#functionality)

